# LeafLang-compiler
This is a programming language which is created by myself.  
Note that this language is still under development.  
## How to Use  
Currently only Syntax Parser is available.  
Run Syntax Parser:  
```shell
cd LeafLang-compiler
python3 Node.py
```
And then, Typing LeafLang Code and `enter` ,you will get the parsed NodeData
## Current Progress
### Syntax Parser
> - [x] Variable Declaration
> - [x] Binary and Unary Operations
> - [x] If Expression
> - [x] Label Expression
> - [x] Function Call
> - [x] CodeBlock
> - [x] Loop Expression
> - [x] With Expression
> - [x] Signal and Emit
> - [x] Basic instances (integer,string,array)
> - [x] Function Declaration
> - [x] Class Definition
> - [x] Import Expression
### Compiler
I am working on it!  
### Executer
Nothing yet too...  

